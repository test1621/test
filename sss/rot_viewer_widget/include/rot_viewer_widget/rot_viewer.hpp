#ifndef rot_viewer_widget_RotViewer_H
#define rot_viewer_widget_RotViewer_H

#include <QWidget>
#include <rviz/visualization_manager.h>
#include <rviz/render_panel.h>
#include <rviz/display.h>
#include <rviz/tool_manager.h>
#include <rviz/view_manager.h>
#include <rviz/view_controller.h>
#include <rviz/properties/property_tree_widget.h>
//-------------------------------------
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

//----------------------------------------
#include "rot_viewer_widget/ui_rot_viewer.h"
// #include <rot_viewer_widget/ui_rot_viewer.h>

namespace rot_viewer_widget
{

using namespace Qt;

class RotViewer : public QWidget
{
    Q_OBJECT

public:
    RotViewer(int argc, char** argv, QWidget *parent = 0);
    ~RotViewer();

    void init_ros(int argc, char **argv);

    void init_display();
    void undisplay();
    void display_grid();
    void display_pc2();
    void display_robot_model();
    void display_axes();
    void display_lines();
    void get_view_info();
    void reset_view();
    void set_tool();
    void set_lines();
    float x_l1,x_h1,y_l1,y_h1,z_l1,z_h1;
private slots:
    void slot_btn_display();
    void slot_btn_quit();
    void slot_btn_test();

private:
    rviz::VisualizationManager *manager_;
    rviz::RenderPanel *render_panel_;
    rviz::ViewManager *view_manager_;
    rviz::ToolManager *tool_manager_;
    rviz::Tool *tools_;
    Ui::RotViewerUI ui;
    
};

};

#endif