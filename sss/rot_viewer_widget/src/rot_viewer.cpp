#include "../include/rot_viewer_widget/rot_viewer.hpp"
#include <QString>
#include <QDebug>
#include <QMessageBox>

namespace rot_viewer_widget
{

RotViewer::RotViewer(int argc, char** argv, QWidget *parent) :
    QWidget(parent)
{
    ui.setupUi(this);

    init_ros(argc, argv);
    

    render_panel_ = new rviz::RenderPanel;
    manager_ = new rviz::VisualizationManager( render_panel_ );
    render_panel_->initialize( manager_->getSceneManager(), manager_ );
    manager_->initialize();
    // view_manager_->initialize();
    //manager_->startUpdate();
    tool_manager_ = manager_->getToolManager();
    tool_manager_ ->initialize();
    view_manager_ = manager_->getViewManager();
    view_manager_->initialize();

    ui.verticalLayout->addWidget( render_panel_ );

    QObject::connect(ui.pushButton_start, SIGNAL(clicked()), this, SLOT(slot_btn_display()));
    QObject::connect(ui.pushButton_quit, SIGNAL(clicked()), this, SLOT(slot_btn_quit()));
    QObject::connect(ui.pushButton_test, SIGNAL(clicked()), this , SLOT(slot_btn_test()));
};

RotViewer::~RotViewer()
{
 
}

void RotViewer::init_ros(int argc, char **argv)
{
    ros::init(argc, argv, "rot_viewer_node", ros::init_options::AnonymousName);
    
}

void RotViewer::init_display()
{
    manager_->removeAllDisplays();
    auto f = manager_->getFixedFrame();
    // ROS_INFO("default fixed frame: %s", f.toStdString().c_str());
    manager_->setFixedFrame("/cloud");

    display_grid();
    display_pc2();
    display_robot_model();
    display_lines();
    set_lines();
    display_axes();
    
}

void RotViewer::display_grid()                                  //显示网格图层
{
    rviz::Display *grid = manager_->createDisplay("rviz/Grid", "adjustable grid",true);
    grid->subProp( "Line Style" )->setValue("Lines");
    grid->subProp("Color")->setValue(QColor(80,160,160));

}

void RotViewer::display_lines()                                 //显示线条图层
{
    rviz::Display *line_list_1 = manager_->createDisplay("rviz/Marker", "adjustable line_list", true);
    line_list_1->subProp("Marker Topic")->setValue("visualization_marker");
    //manager_->startUpdate();
}



void RotViewer::display_pc2()                                   //显示点云图层
{
    rviz::Display *pc2 = manager_->createDisplay("rviz/PointCloud2", "adjustable pointcloud2", true);
    pc2->subProp("Topic")->setValue("/rot_filtered");
    pc2->subProp("Size (m)")->setValue("0.03");
}

void RotViewer::display_robot_model()                           //显示机器人模型图层
{
    rviz::Display *robot = manager_->createDisplay( "rviz/RobotModel", "adjustable robot", false );
    robot->subProp( "Robot Description" )->setValue( "robot_description" );
}

void RotViewer::display_axes()                                  //显示坐标系图层
{
    rviz::Display *ax = manager_->createDisplay("rviz/Axes", "ax", true);
    ax->subProp("Length")->setValue("0.5");
    ax->subProp("Radius")->setValue("0.05");
    manager_->startUpdate();
}
void RotViewer::undisplay()
{
    manager_->removeAllDisplays();
    auto f = manager_->getFixedFrame();
    // ROS_INFO("default fixed frame: %s", f.toStdString().c_str());
    manager_->setFixedFrame("/cloud");
    rviz::Display *ax = manager_->createDisplay("rviz/Axes", "ax", false);
    rviz::Display *line_list_1 = manager_->createDisplay("rviz/Marker", "adjustable line_list", false);
    rviz::Display *grid = manager_->createDisplay("rviz/Grid", "adjustable grid",false);
}
void RotViewer::slot_btn_display()
{
    static int i = 0;

    if(i%2 == 0)
    {
        init_display();
        
        manager_->startUpdate();
        ui.pushButton_start->setText("NonDisplay");
    }
    else if(i%2 == 1)
    {
        undisplay();
        manager_->startUpdate();
        //manager_->stopUpdate();
        ui.pushButton_start->setText("Display");
    }
    i++;
}

void RotViewer::slot_btn_quit()
{
    this->close();
}

void RotViewer::slot_btn_test()
{
    get_view_info();
}

void RotViewer::reset_view()
{
    auto *vc = view_manager_->getCurrent();
    vc->reset();
    ROS_INFO("reset view");
}

void RotViewer::get_view_info()
{
    auto *vc = view_manager_->getCurrent();
    auto *model = view_manager_->getPropertyModel();
    auto *cam = vc->getCamera();
    auto orient = cam->getOrientation();
    auto pos = cam->getPosition();
    std::cout << "camera orientation: " << orient << "\n";
    std::cout << "camera position: " << pos << "\n";
}

void RotViewer::set_tool()
{
    tools_ = tool_manager_->addTool("rviz/FocusCamera");
    tool_manager_->setCurrentTool(tools_);
    manager_->startUpdate();
}
/*
void poseCallback(const rot_viewer_widget::BoundingBox msg)
{
    // 将接收到的消息打印出来
    ROS_INFO("Turtle pose: x:%f, y:%f", msg.x_hi, msg.x_lo);
}
*/
void RotViewer::set_lines()     //画线
{
    ros::NodeHandle nh,n;
    //ros::NodeHandle n("~rot_lidar");
    ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 5);             //topic
    //ros::Subscriber pose_sub = n.subscribe("/rot_lidar/detect_range", 10, poseCallback);
    visualization_msgs::Marker line_list,line_strip,points,cylinder;
    //visualization_msgs::MarkerArray lines;
    points.header.frame_id = line_list.header.frame_id = line_strip.header.frame_id = "/cloud";                               //FixedFrame
    points.header.stamp = line_list.header.stamp = line_strip.header.stamp = ros::Time::now();
    points.ns = line_list.ns = line_strip.ns = "rot_viewer_node";
    points.action = line_list.action = line_strip.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = line_list.pose.orientation.w = line_strip.pose.orientation.w = 1.0;
    line_list.id = 0;                   //id
    line_strip.id = 1;
    points.id  = 2;
    line_list.type = visualization_msgs::Marker::LINE_LIST;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 0.01;
    points.scale.y = 0.01;
    points.color.g = 1.0f;
    points.color.a = 1.0;    
    line_list.scale.x = 0.02;                                                       //大小
    line_list.scale.y = 0.02;
    line_strip.scale.x = 0.02;
    line_strip.scale.y = 0.02;
    line_list.color.b = 1.0;                                                                            //颜色
    line_list.color.a = 1.0;                                                                             //透明度
    line_strip.color.b = 1.0;
    line_strip.color.a = 1.0;
    //rot_viewer_widget::BoundingBox msg;

    double x_lo,x_hi,y_lo,y_hi,z_lo,z_hi;
    double x_l ,x_h,y_l,y_h,z_l,z_h;
    if(nh.hasParam("/rot_lidar/detect_range/x_lo"))
    {
        x_l = x_lo;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    if(nh.getParam("/rot_lidar/detect_range/x_hi", x_hi))
    {
      x_h = x_hi;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    if(nh.getParam("/rot_lidar/detect_range/y_lo", y_lo))
    {
      y_l = y_lo;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    if(nh.getParam("/rot_lidar/detect_range/y_hi", y_hi))
    {
      y_h = y_hi;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    if(nh.getParam("/rot_lidar/detect_range/z_lo", z_lo))
    {
      z_l = z_lo;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    if(nh.getParam("/rot_lidar/detect_range/z_hi", z_hi))
    {
      z_h = z_hi;
        ROS_INFO("yes");
    }else{
        ROS_INFO("no");
    }
    ROS_INFO(" pose: xl:%f  xh:%f  yl:%f  yh:%f  zl:%f  zh:%f",x_lo,x_hi,y_lo,y_hi,z_lo,z_hi);
    ROS_INFO(" pose: xl:%f  xh:%f  yl:%f  yh:%f  zl:%f  zh:%f",x_l,x_h,y_l,y_h,z_l,z_h);
  //设置框的范围
      //右上

      float x_long = x_h-x_l;
      float y_long = y_h-y_l;
      float z_long = z_h-z_l;
   for (float i = 0.01; i <= x_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l+i;
      p.y = y_h;
      p.z = z_h;

      points.points.push_back(p);
        i+=0.01;
    }

   for (float i = 0.01; i <= z_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l;
      p.y = y_h;
      p.z = z_h-i;

      points.points.push_back(p);
        i+=0.01;
    }

    for (float i = 0.01; i <= y_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l;
      p.y = y_h-i;
      p.z = z_h;

      points.points.push_back(p);
        i+=0.01;
    }
    //右下
    for (float i = 0.01; i <= x_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h-i;
      p.y = y_h;
      p.z = z_l;

      points.points.push_back(p);
        i+=0.01;
    }

    for (float i = 0.01; i <= z_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h;
      p.y = y_h;
      p.z = z_l+i;

      points.points.push_back(p);
        i+=0.01;
    }
    for (float i = 0.01; i <= y_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h;
      p.y = y_h-i;
      p.z = z_l;

      points.points.push_back(p);
        i+=0.01;
    }
    //左上
    for (float i = 0.01; i <= x_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h-i;
      p.y = y_l;
      p.z = z_h;

      points.points.push_back(p);
        i+=0.01;
    }
    for (float i = 0.01; i <= z_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h;
      p.y = y_l;
      p.z = z_h-i;

      points.points.push_back(p);
        i+=0.01;
    }
    for (float i = 0.01; i <= y_long; )
    {
      geometry_msgs::Point p;
      p.x = x_h;
      p.y = y_l+i;
      p.z = z_h;

      points.points.push_back(p);
        i+=0.01;
    }
    //左下
    for (float i = 0.01; i <= x_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l+i;
      p.y = y_l;
      p.z = z_l;

      points.points.push_back(p);
        i+=0.01;
    }
    for (float i = 0.01; i <= z_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l;
      p.y = y_l;
      p.z = z_l+i;

      points.points.push_back(p);
        i+=0.01;
    }
    for (float i = 0.01; i <= y_long; )
    {
      geometry_msgs::Point p;
      p.x = x_l;
      p.y = y_l+i;
      p.z = z_l;

      points.points.push_back(p);
        i+=0.01;
    }
    marker_pub.publish(points);
    marker_pub.publish(line_strip);
    marker_pub.publish(line_list);
}
    
} // namespace rot_viewer_widget