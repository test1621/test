#include <rot_viewer_widget/rot_viewer.hpp>
#include <QApplication>

int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    // ros::init(argc, argv, "rot_viewer_node");
    rot_viewer_widget::RotViewer w(argc, argv);
    w.show();

    return a.exec();
}
